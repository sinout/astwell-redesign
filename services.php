<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft | Services</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body>
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default navbar-fixed-top">
		      <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
          	<img src="img/logo.png" class="img-responsive">
        	</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li><a href="portfolio.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
            <li class="active"><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
		<div class="inner-hero-unit">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>From idea to implementation!</h1>
						<div class="row">
							<div class="col-md-12">
								<p>Our team of designers and developers is constantly looking for new modern tendencies to guarantee our products usage being sleek and natural. Our projects are individual and progressive. Each web design is distinctive as we consider the website or application auditory, determining the different types of users and their purposes to work out the ideal experience for each user type.</p>
							</div>
						</div>
					</div>
				</div> <!--row -->
			</div>
		</div>
    <div class="services-content">    
			<div class="business-content" id="business">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-12 col-md-offset-0 col-lg-4 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0">
							<div class="service-heading">
								<p class="business-section-ico"></p>
								<p class="service-heading-text text-left">
									BUSINESS SYSTEMS <br> & TOOLS</p>
							</div>
						</div>
						<div class="col-md-7 col-lg-6 col-sm-12">
							<div class="service-description">
								<p>We build B2B and B2C solutions that meet all the challenges of the competitive landscape; that create organic communication between client and brand. Our business applications are fully integrated with social media to deliver experiences and convert visitors into users.</p>
								<p>To meet all the requirements of complex operating systems and database needs we use PHP, Java, Javascript, HTML5, CSS3, My SQL and a range of other technologies.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
				<div class="ecommerce-content w-border" id="ecommerce">
					<div class="g-mask"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-7 col-sm-12 col-md-offset-0 col-lg-6 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0">
								<div class="service-description text-right">
									<p>Your own e-commerce website can be your online startup or a great helper to your offline business as it’s accessible and convenient for everyone.  Automatic tax calculation and easy fulfillment systems create positive attitude to your brand – right what you need to increase sales and profits.</p>
									<p>Astwellsoft will help you not only to build an online store from scratch but to convert the possible online shoppers into loyal customers.</p>
								</div>
							</div>
							<div class="col-md-5 col-sm-12 col-lg-4">
								<div class="service-heading">
									<p class="ecommerce-section-ico rotated"></p>
									<p class="service-heading-text"><span>E-COMMERCE</span> <span>PLATFORMS</span></p>
								</div>							
							</div>
						</div>
					</div>
				</div>
					<div class="social-content" id="social">
						<div class="g-mask"></div>
						<div class="container">
							<div class="row">
								<div class="col-md-4 col-sm-12 col-md-offset-0 col-lg-offset-1 col-lg-3 
								col-sm-offset-0 col-xs-offset-0">
									<div class="service-heading text-right">
										<p class="social-section-ico clearfix"></p>
										<p class="text-right service-heading-text"><span>SOCIAL</span> <span>PLATFORMS</span></p>
									</div>
								</div>
								<div class="col-lg-1 hidden-md hidden-sm hidden-xs"></div>
								<div class="col-md-8 col-sm-12 col-lg-6">
									<div class="service-description">
										<strong>Astwellsoft team will develop a custom social network/online community/dating website with great user experience, innovative features and all required functionality as we are highly experienced in this niche.</strong>
										<p>We provide solutions by using a Content Management System built upon a LAMP (Linux, Apache, MySQL, PHP) platform, what ensures such features as:</p>
										<ul>
											<li>user profiles with ability to present photos, videos, fill the personal information, select the interests that are used for match-finding;</li>
											<li>user search based on interests, sex, location and other parameters;</li>
											<li>user messaging (including video messages);</li>
											<li>subscription functionality that grants users different possibilities based on the chosen plan;</li>
											<li>payment API integration, etc.</li>
										</ul>
									</div>							
								</div>
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="mobile-content w-border" id="mobile">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-sm-12 col-lg-6 col-lg-offset-1">
									<div class="service-description text-right">
										<p>Our team provides end-to-end services in the mobile application development that work consistently across various Android and iOS devices. </p>
										<p>Our mobile applications response all unique customer’s requirements and attract varied audiences of users what is very important for startups and companies that want to settle their position on market.</p>
										<p>IOS Apps. We develop the mobile apps that are lightly to use on iPhone & iPad devices and are organic with company`s native apps</p>
										<p>Android Apps. We create applications that fit on any Android device, regardless of the manufacturer</p>
									</div>							
								</div>
								<div class="col-lg-1 hidden-md hidden-xs hidden-sm"></div>
								<div class="col-md-4 col-sm-12 col-lg-3">
									<div class="service-heading">
										<p class="mobile-section-ico"></p>
										<p class="service-heading-text"><span>MOBILE</span> <span>APPLICATIONS</span></p>
									</div>
								</div>								
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="design-content" id="design">
						<div class="g-mask"></div>
						<div class="container">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-lg-2 col-lg-offset-2">
									<div class="service-heading">
										<p class="design-section-ico"></p>
										<p class="service-heading-text text-right">
											<span>Digital</span>
											<span>design</span>
										</p>
									</div>
								</div>
								<div class="col-md-9 col-sm-12 col-lg-6">
									<div class="service-description">
										<p>Our team of designers is constantly looking for new modern tendencies to guarantee our products usagebeing sleek and natural. Our projects are individual and progressive. Each web design is distinctive as we consider the website or application auditory, determining the different types of users and their purposes to work out the ideal experience for each user type.</p>
									</div>							
								</div>
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="branding-content" id="branding">
						<div class="container">
							<div class="row">
								<div class="col-md-6 col-sm-12 col-lg-5 col-lg-offset-1">
									<div class="service-description text-right">
										<strong>We create Names. We create Brands. We create unique corporate stylistics due to customer`s business objectives through a collaborative design process, analytics, creativity and emotion.</strong>
									</div>							
								</div>
								<div class="col-md-6 col-sm-12 col-lg-5">
									<div class="service-heading">
										<p class="branding-section-ico"></p>
										<p class="service-heading-text">Identity <br>& Branding</p>
									</div>
									<div class="subheading-text">
										<p>Our Identity and Branding Services include:</p>
										<ul>
											<li>Brand Story Development</li>
											<li>Identity Design and Logo</li>
											<li>Print Design</li>
											<li>Branded Collateral</li>
										</ul>
									</div>
								</div>								
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="startups-content w-border" id="startups">
						<div class="g-mask"></div>
						<div class="container">
							<div class="row">
								<div class="col-md-5 col-sm-12 col-lg-4 col-lg-offset-1">
									<div class="service-heading">
										<p class="startups-section-ico"></p>
										<p class="service-heading-text">
											<span>Technical support</span>
											<span>for startups</span>
										</p>
									</div>
								</div>
								<div class="col-md-7 col-sm-12 col-lg-6">
									<div class="service-description">
										<p>Startups are the projects we like the most! We are capable to convert the idea to an individual product that suits precisely to your target audience.</p>
										<p>Successful startup is not only about having a good idea. It’s also an experience that comes up while exploring the product. And that must feel excellent. Our team is constantly following modern tendencies to guarantee your products usage being sleek and natural, being technically flawless at the same time.</p>
									</div>							
								</div>
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="support-content" id="support">
						<div class="g-mask"></div>
						<div class="container">
							<div class="row">
								<div class="col-md-4 col-sm-12 col-lg-4 col-lg-offset-2 col-md-offset-1">
									<div class="service-description text-right">
										<strong>We never stop maintaining our products and that’s why all our clients became our friends and good partners.</strong>
									</div>							
								</div>
								<div class="col-md-6 col-sm-12 col-lg-4">
									<div class="service-heading">
										<p class="support-section-ico"></p>
										<p class="service-heading-text">Support of existing projects</p>
									</div>
									<div class="subheading-text">
										<p>We provide:</p>
										<ul>
											<li>Website editing, revisions, version updating</li>
											<li>Creating new graphics</li>
											<li>Modifying CSS styles and code</li>
											<li>Bug fixing and problem reporting</li>
											<li>Offering advices and guidance on website management</li>
										</ul>
									</div>
								</div>								
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
					<div class="data-content w-border" id="data">
						<div class="container">
							<div class="row">
								<div class="col-md-4 col-sm-12 col-lg-4 col-lg-offset-2 col-md-offset-1">
									<div class="service-heading ">
										<p class="data-section-ico"></p>
										<p class="service-heading-text"><span>Servers & Data</span></p>
									</div>
								</div>
								<div class="col-md-6 col-sm-12 col-lg-4">
									<div class="service-description">
										<strong>Astwellsoft provides not only turn-key solutions for businesses but also protects and maintains all important data.</strong>
										<p>Our custom solutions:</p>
										<ul>
											<li>Server Maintenance</li>
											<li>Storage Maintenance</li>
											<li>Data Center Relocations</li>
										</ul>
									</div>							
								</div>																
							</div> <!-- row -->
						</div> <!-- container -->							
					</div> <!-- social content -->
		
			</div>
		
		<?php include 'patrials/callout.html'; ?>
		<?php include 'patrials/footer.html'; ?>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
	</body>
</html>	