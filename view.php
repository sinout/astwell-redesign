<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft | Portfolio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/animate.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body id="view-app">
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default light-menu navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/logo-inverted-no-space.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li class="active"><a href="portfolio.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
		<div class="project-view-section" >
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<h1 class="project-title v-anim animated fadeIn">{{json.name}}</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-sm-12">
						<p class="project-subtitle v-anim animated fadeIn">{{json.subtitle}}</p>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="project-switcher">
							<div class="row">
								<div class="col-md-7 col-sm-3 col-xs-6 text-left">
									<a href="#" v-on:click="changeProject"> <i class="fa fa-angle-left"></i><span id="prev">Previous Project</span></a>
								</div>
								<div class="col-md-5 col-sm-3 col-xs-6 text-right">
									<a href="#" v-on:click="changeProject"><span id="next">Next project</span><i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div> <!-- end of portfolio section -->
		<div class="current-project">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-12">
						<div class="current-project-descr">
							<h3>PROJECT DESCRIPTION</h3>
							<p class="v-anim animated fadeIn">{{json.description.paragraph}}</p>
							<ul type="square" class="v-anim animated fadeIn" v-if="json.description.createList">
								<li  v-for="item in json.description.list"><span>{{item}}</span></li>
							</ul>
							<a v-bind:href="json.url" v-bind:class="[ json.url ? '' : 'hidden']" class="btn btn-default link-to-project">Live Project</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="current-project-artworks" v-bind:class="{'bx-controls-hidden': json.noControls}">
							<h3>Project artworks</h3>
							<ul class="artworks-slides v-anim animated fadeIn"></ul>
							<div class="technologies-used">
								<h3>Technologies used</h3>
								<p class="v-anim animated fadeIn">{{json.technologies}}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include 'patrials/callout.html'; ?>
		<?php include 'patrials/footer.html'; ?>

<!-- Modal -->
<div class="modal fade" id="bxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

		

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/vue.min.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<script src="js/view.js"></script>

	</body>
</html>	