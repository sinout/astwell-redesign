<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft | Portfolio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body>
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default navbar-fixed-top">
		      <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/logo.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li><a href="portfolio.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="services.php">Services</a></li>
            <li class="active"><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
		<div class="inner-hero-unit">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>From idea to implementation!</h1>
						<div class="row">
							<div class="col-md-6">
								<p>We will be glad to hear you soon!</p>
							</div>
						</div>
					</div>
				</div> <!--row -->
			</div>
		</div>
		<section class="contacts">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>Contact us</h1>
						<form id="contact-message" action="">
							<div class="thanks">
								<h3>Thank you for message. We'll contact soon !</h3>
							</div>
							<div class="form-group">
								<input type="text" name="name" placeholder="Your Name">
							</div>
							<div class="form-group">
								<input type="text" name="email" placeholder="Your Email">
							</div>
							<div class="form-group">
								<textarea name="message" placeholder="Please Enter Your Message"></textarea>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-default">Send Message</button>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<h1>Visit us</h1>
						<div class="tab-container">

						  <!-- Tab panes -->
						  <div class="tab-content">
						  	<div class="tab-mask"></div>
						    <div role="tabpanel" class="tab-pane  active" id="office-1">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Mississauga, ON, L4W 5B2, Canada</p>
						    	<p>2600 Skymark Ave</p>
						    	<p>+1 416 262 0774</p>						    	
						    </div>
						    <div role="tabpanel" class="tab-pane" id="office-2">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Lviv, Ukraine</p>
						    	<p>73 Heroyiv Upa St., Office 302, 79000</p>
						    	<p>+38 098 343 9808</p>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="office-3">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Chernivtsi, Ukraine</p>
						    	<p>2 Koziubynskoho St., 58000</p>
						    	<p>+38 096 444 45 43</p>
						    </div>
						  </div>	

						  <!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active"><a href="#office-1" aria-controls="office-1" role="tab" data-toggle="tab">Toronto, Canada</a></li>
						    <li role="presentation" ><a href="#office-2" aria-controls="office-2" role="tab" data-toggle="tab">Lviv, Ukraine</a></li>
						    <li role="presentation"><a href="#office-3" aria-controls="office-3" role="tab" data-toggle="tab">Chernivtsi, Ukraine</a></li>
						  </ul>
						
						</div>
					</div>
				</div> <!--row -->

				<?php include 'patrials/social-block.html'; ?>
			</div>
		</section>
		
		<?php include 'patrials/footer.html'; ?>


		
	
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
	<script src="js/send-form.js"></script>

	</body>
</html>	