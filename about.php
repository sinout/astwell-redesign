<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft | Portfolio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body>
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default navbar-fixed-top">
		      <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/logo.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li><a href="portfolio.php">Projects</a></li>
            <li class="active"><a href="about.php">About</a></li>
            <li><a href="services.php">Services</a></li>
            <li ><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
		<section class="about section inner-hero-unit">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>From idea to implementation!</h1>
						<div class="row">
							<div class="col-md-12">
								<p>Your website or mobile application is the direct reflection of your business.  At the moment the user opens it, he decides whether he will be your consumer or not. Your application should be ideally crafted to details to win his trust and form long reliable relationship. Astwellsoft is the one who will help you with this!</p>
							</div>
						</div>
					</div>
				</div> <!--row -->
			</div>
		</section>
		<section class="links portfolio-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-10 col-lg-offset-4 col-md-offset-2 text-right">
						<ul class="project-nav filter-button-group text-uppercase">
							<li class="active"><a href="#we" >WE ARE</a></li>
							<li><a href="#company">WHY ASTWELLSOFT?</a></li>
							<li><a href="#wprocess">HOW WE DO IT?</a></li>
							<li><a href="#benefits" >BENEFITS</a></li>
							<li><a href="#solutions">WE OFFER</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="we" class="about-section we">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading text-uppercase">We Are</h3>
						<div class="row">
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>Established in</p>
										<h4>2014</h4>
										<p class="text-uppercase">YEAR</p>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we scisors'></div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>Head office in</p>
										<h4>LVIV,</h4>
										<p class="text-uppercase">UKRAINE</p>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we marker'></div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>Currently have</p>
										<h4>20+</h4>
										<p class="text-uppercase">EMPLOYEES</p>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we user'></div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>More than</p>
										<h4>100</h4>
										<p class="text-uppercase">PROJECTS</p>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we desk'></div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>More then</p>
										<h4>70%</h4>
										<p class="text-uppercase">CUSTOMER SATISFACTION</p>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we smile'></div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 cell">
								<div class="row">
									<div class="col-lg-7 col-md-8 col-xs-6 text-right">
										<p>Main clients are in:</p>
										<h4>USA, CANADA, AUSTRALIA</h4>
									</div>
									<div class="col-lg-5 col-md-4 col-xs-6">
										<div class='we map'></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="company" class="about-section diff-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading text-uppercase">WHY ASTWELLSOFT?</h3>
						<div class="row">
							<div class="col-md-9 col-md-offset-2">
								<p>
									In the modern business environment, where everything is changing instantly,companies have to not only offer quality products and services, but also to use any available tools in order to survive and get in front of competitors. In this key, one of the most valuable instruments today is Software, that enables companies to manage multiple processes and analyze information quickly, efficiently and at the right time. Proper Software solutions can be the crucial tool for companies to keep the processes controlled &amp; organized, have access to the structured &amp; operable insights, &amp; have a strong base for right decision making.
								</p>
								<p>
									Astwellsoft understands the value that properly engineered software can bring into your business. It also gives us a sense of responsibility for the products we develop for our clients. We carefully analyze your issues &amp; related business processes to provide software solutions that are suitable in each separate case. Our experience also allows us to provide our clients with functionality suggestions that might make the solution even more valuable for your company. Astwellsoft treats each client as a long-term partner, making sure that each developer Solution is high-performing, scalable and robust, integrating easily into existing business processes and strengthen company’s overall infrastructure.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="wprocess" class="about-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading text-uppercase">HOW WE DO IT?</h3>
						<div class="row">
							<div class="col-md-4">
								<h4 class="text-uppercase blue">We work collaboratively with our clients.</h4>
								<h4 class="text-uppercase blue">Astwellsoft establishes partnerships not client vendor relationships.</h4>
							</div>
							<div class="col-md-8">
								<ul>
									<li>Engage our experts (Senior Project Managers, and Senior Business Analysts) to work directly with the client.</li>
									<li>Keep cost and quality always in focus -- We focus on delivering it right the first time as opposed to reworking to get to the right solution.</li>
									<li>Ensure client participation throughout the engagement cycle.</li>
									<li>Set up checks and balances in our systems and processes to ensure quality and timely deliveries.</li>
									<li>Provide post-implementation support.</li>
								</ul>
								<p>
									Our projects comply with clearly defined delivery metrics and SLAs. Our professionals provide project management expertise, integrated project plans and change management. We ensure that our clients have full visibility into the projects and project teams through multiple touch points.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>		
		<section id="benefits" class="about-section bnfts">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading text-uppercase">Benefits</h3>
					</div>
					<div class="col-md-12">
						<!-- (.new-5-col>.row>.col-md-12.rb>p{sd})*10 -->
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits gt'></div>
									</div>
									<p>We are a great team of experienced professionals who care about a client's business.</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits fc'></div>
									</div>
									<p>Flawless communication at every stage.</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits hlq'></div>
									</div>
									<p>The highest levels of quality that focuses on customer satisfaction</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits tec'></div>
									</div>
									<p>Technical excellence combined with strong management skills</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits skb'></div>
									</div>
									<p>A superior knowledge base and business expertise to deliver complex ready-to-market solutions</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits srm'></div>
									</div>
									<p>A strong management team with extensive experience working with European, Australian and US clients</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits tm'></div>
									</div>
									<p>A team of 21 skilled IT specialists with a wide range of technical skills</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits eq'></div>
									</div>
									<p>Exceptional quality, competitive cost, and rapid delivery guarantee excellent value</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits pb'></div>
									</div>
									<p>A proven and demonstrable track record of success</p>
								</div>
							</div>
						</div>
						<div class="new-5-col">
							<div class="row">
								<div class="col-md-12 rb ben-h">
									<div class="ben">
										<div class='benefits ltt'></div>
									</div>
									<p>We build long-term teams and we build long-term relationships</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<!-- 		<section id="technologies" class="about-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading upcase">Technologies</h3>
						<div class="row">
							<div class="col-md-12">
								<div class="new-5-col">
									<p class="text-center big">01</p>
									<div class="row">
										<div class="col-md-12 rb tech-h">
											<p>Proficient in PHP and MySQL. Strong experience with the LAMP (Linux, Apache, MySQL, PHP)</p>
										</div>
									</div>
								</div>
								<div class="new-5-col">
									<p class="text-center big">02</p>
									<div class="row">
										<div class="col-md-12 rb tech-h">
											<p>Expert knowledge and experience with leading PHP MVC frameworks - Yii/Yii2, Codelgniter, CakePHP, Zend and content management systems - WordPress, MODx, Magento, OpenCart, PrestaShop</p>
										</div>
									</div>
								</div>
								<div class="new-5-col">
									<p class="text-center big">03</p>
									<div class="row">
										<div class="col-md-12 rb tech-h">
											<p>Expert knowledge and experience with Javascript, jQuery, CSS, HTML, Bootstrap, AJAX with responsive design</p>
										</div>
									</div>
								</div>
								<div class="new-5-col">
									<p class="text-center big">04</p>
									<div class="row">
										<div class="col-md-12 rb tech-h">
											<p>Proficient with JSON, LESS/SAAS, Angular.js</p>
										</div>
									</div>
								</div>
								<div class="new-5-col">
									<p class="text-center big">05</p>
									<div class="row">
										<div class="col-md-12 tech-h">
											<p>RESTful API creation and integration</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<section id="solutions" class="about-section solutions">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="heading text-uppercase">we offer</h3>
						<div class="row">
							<div class="col-md-4">
								<h4 class="text-uppercase blue">
									Astwellsoft creates state-of-the-art software solutions for early-stage technology companies and new product development arms of major global corporations.
								</h4>
							</div>
							<div class="col-md-8">
								<ul>
									<li>We adopt a tried &amp; tested Project Management process to help co-create the best products with you.</li>
									<li>Our proven Project Management solutions help build your path from idea to product.</li>
									<li>At Astwellsoft, we are passionate about bringing design and technology together to create beautiful and smart product solutions.</li>
									<li>Software Engineering is core to all of our Astwellsoft service offerings and 80% of our projects encompass a software engineering component.</li>
								</ul>
								<p>
									The Astwellsoft team has broad experience developing great web solutions across the entire technology and business spectrum. We want to work with you to create web solutions that excel in all key areas such as web technologies, UX design, usability, scalability and interoperability
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		<?php include 'patrials/callout.html'; ?>
		
		<?php include 'patrials/footer.html'; ?>



		

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
	</body>
</html>	