$(document).ready(function() {


	var menu = $('nav'),
			footer = $('.footer-large'),
			owlpages = $('.owl-page'),
			tooltips = $('.tooltip'),		
			currentIndex;
	var slug = window.location.pathname;

	switch(slug){
		case '/' :

			$('#fullPage').fullpage({
				scrollBar: true,
				responsiveWidth: 1209,
				responsiveHeight: 630,
				verticalCentered: false,
				fitToSectionDelay: 300
			});

			// owl carousel init
			var owl = $("#owl").owlCarousel({
				autoplay: true,
				autoplayTimeout: 3000,
		 		item: 1,
		 		loop:true,
		    nav: false,
				responsive: {
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        },
		        2000: {
		        	items: 1
		        }
		     }   
			});

			owl.owlCarousel();
			owl.on('changed.owl.carousel', function(event) {
				switchMapMarker(event);
			});

			//scroll-to for mouse icon
			$('.hero-mouse a').on('click', function () {
				$('#services').ScrollTo({
					duration: 1000
				});
				return false;
			});

		break;
	}

	//changing navbar logo onscroll
	
  $(window).scroll(function () {

	    var z = menu.offset().top;
	    var brand = menu.find('.navbar-brand img');
	    if (z > 20){
	      menu.addClass('light-menu');
	    	brand.attr('src', '../img/logo_inverted.png').css('max-width', '180px');
	    }
	    else{
	      menu.removeClass('light-menu');
	    	brand.attr('src', '../img/logo.png');
	  	}

  });


	function switchMapMarker (event) {
		tooltips.fadeOut();
		currentIndex = event.item.index - 3;
		console.log(currentIndex);
		tooltips.each(function(index, el) {
			if ($(el).hasClass('slide'+currentIndex)) {
				$(this).fadeIn();
			}
		});			
	}


  
});