$(document).ready(function() {


		var valid = false,
			form = $('#contact-message');

	$.validator.setDefaults({
    highlight: function(element) {
        $(element).addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).removeClass('has-error');
    }

  });
		form.validate({
			rules :{
				name: "required",
				email: {
					required: true,
					email: true
				},
				message: {
					required: true,
					minlength: 5
				}
			},
			submitHandler: function () {
				ajaxSubmit();
			}
		});

		function ajaxSubmit () {
			var formData = form.serialize();
			$.ajax({
				type : 'POST',
				url : '../process.php',
				data : formData,
				dataType: 'json',
				encode: true
			})
			
			.done(function (data) {
				if(data){
					form.addClass('rotated');
					form.find('.thanks').fadeIn('fast');
					setTimeout(function() {
						form.find('input, textarea').val('');
						form.removeClass('rotated').find('.thanks').fadeOut('fast');
					}, 5000);
				}

			});			
		}		



});