$(document).ready(function() {

	var vm = new Vue({
		el: '#view-app',
		
		data : {
			fulljson: [],
			json: [],
			current: 0,
			bx: null,
			bxSettings: {
				auto : true,
				mode : 'fade',
				pause: 3000,
				pager: false,
				adaptiveHeight: true,
				preventDefaultSwipeX: true
			}, 
		},

		ready: function () {
			this.loadData();
			this.getHistoryName();
			this.$nextTick(function () {
				this.updateCarousel();
				this.initCarousel();
			});
		},

		methods: {

			loadData: function  () {
				this.fulljson = (function () {
			    var json = null;
			    $.ajax({
			      'async': false,
			      'global': false,
			      'url': 'projects.json',
			      'dataType': "json",
			      'success': function (data) {
			          json = data;
			      }
			    });
			    return json;
				})();				
			},

			getHistoryName : function  (skipSession) {
				if(skipSession){
					this.$set('json', this.fulljson[this.current]);
				}
				else{
					var project = sessionStorage.getItem('historyItem');
					this.render(project);
				}

			},
			render : function (item) {
				var founded = false;
				for (var i = 0; i < this.fulljson.length; i++) {
					
					if (founded)
						break;
					
					for(var key in this.fulljson[i]){
						
						if (founded)
							break;

						if (this.fulljson[i].hasOwnProperty(key)) {
								var currkey = this.fulljson[i]['name'];
							
							if(currkey.toLowerCase() === item.toLowerCase()){
								this.$set('json', this.fulljson[i]);
								this.current = i;
								founded = true;
							}

						}

					}

				}
				
		},

			changeProject : function  (e) {
				var clickedElem = $(e.target).attr('id');
				this.current = this.checkAccessability(clickedElem);
				var skipSession = true;
				sessionStorage.setItem('historyItem', this.fulljson[this.current].name);
				this.getHistoryName(skipSession);
				this.$nextTick(function  () { //finished dom rendering
					this.updateCarousel();
					this.reloadCarousel();
					this.reloadAnim();
				});
			},

			checkAccessability: function (clickedElem) {
				var value = this.current;
				switch(clickedElem){
					case 'next':
						if(this.fulljson.length > this.current + 1)
							value++;
						break;
					case 'prev':
						if(this.current - 1 >= 0)						
							value--;
						break;
				}
				return value;
			},

			updateCarousel: function () {
				var content = "",
						slides = $('.artworks-slides');
				for( var i = 0; i < this.json.slides.length; i++){
					content+= "<li><img data-modal-slides='"+ this.json.modalSlidesName+"' src='" + this.json.slides[i] + "'></li>";
				}
				slides.empty().append(content);
			},
			initCarousel: function  () {
				bx = $('.artworks-slides').bxSlider(this.bxSettings);
			},

			reloadCarousel : function  () {
				bx.reloadSlider(this.bxSettings);
			},

			reloadAnim : function () {
				var element = $('.v-anim');
				element.removeClass('animated fadeIn');
				element.width();
				element.addClass('animated fadeIn');				
			}

		}

	});

	$('.artworks-slides').on('click', 'img', function(event) {
		event.preventDefault();

		var thisImage = $(event.target).data('modal-slides'),
				modalSlides = vm.$get('json'),
				content = '',
				string = thisImage+'_modalSlides',
				container = $('#bxModal .carousel-inner');
		container.empty();
			for (var i = 0; i < modalSlides[string].length; i++) {
				content = "<div class='item'><img class='img-responsive' src='" + modalSlides[string][i] + "'></div>";
				$(content).appendTo('.carousel-inner');
			};
		$('.item').first().addClass('active');
		$('.carousel').carousel();
		$('#bxModal').modal('show');

	});

});

