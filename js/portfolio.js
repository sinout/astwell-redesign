var vm =	new Vue({
		
		el: "#app",
		data : {
			fulljson: [],
			projects: [],
			currentClass : null
		},

		ready: function  () {
			this.loadData();
		},

		methods : {
			loadData : function  () {
				this.fulljson = (function () {
			    var json = null;
			    $.ajax({
			      'async': false,
			      'global': false,
			      'url': 'projects-preview.json',
			      'dataType': "json",
			      'success': function (data) {
			          json = data;
			      }
			    });
			    return json;
				})();
				this.$set('projects', this.fulljson);
				this.$nextTick(function () {
					this.setItemsHegiht();
					this.initGrid();
					this.checkForHash();
					window.addEventListener('hashchange', this.checkForHash);
					window.addEventListener('resize', this.setItemsHegiht);
				});							
			},

			initGrid: function () {
				this.$grid = $('.grid').isotope({
			    itemSelector: '.grid-item',
			    percentPosition: true,
			    masonry: {
			      columnWidth: '.grid-sizer'
			    }
				});
				
			},
			setItemsHegiht : function () {
				var portfolioHeight = $('.portfolio-section').innerHeight(),
						windowHeight = $(window).innerHeight(),
						itemHeight = windowHeight - portfolioHeight;
				$('.grid-item').height(itemHeight/2);
				$('.grid-item--height2').height(itemHeight);			
			},

			sortItems: function (event) {
				var filterValue = $(event.target).attr('data-filter');
				$('.project-showing').html('Showing ' + $(event.target).text() + ' projects');
				$('.filter-button-group').find('li').removeClass('active');
				$('.grid').isotope({filter: filterValue});
				$(event.target).parent().addClass('active');				
			},
			
			openProject: function (name, event) {
				sessionStorage.setItem('historyItem', name);
			},

			checkForHash: function () {
				var hashFilter = window.location.hash;
				if(hashFilter !== ''){
					hashFilter = hashFilter.replace(/[#]+/, '');
					this.$grid.isotope({filter: '.'+hashFilter});
					var link = $('.filter-button-group').find("[data-filter='." + hashFilter + "']");
					$('.project-showing').html('Showing ' + link.text() + ' projects');
					$('.filter-button-group').find('li').removeClass('active');
					link.parent().addClass('active');
					$('html,body').scrollTop(0);
				}				
			},

		}
	
	});
