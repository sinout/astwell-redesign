<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft | Portfolio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body id="app">
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default light-menu navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/logo-inverted-no-space.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li class="active"><a href="portfolio.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
		<div class="portfolio-section" >
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<h1>Our Work</h1>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<p class="project-showing">Showing all projects</p>
					</div>
					<div class="col-md-8 col-sm-12 text-right">
						<ul class="project-nav filter-button-group">
							<li class="active"><a href="#" data-filter="*" @click="sortItems($event)">All</a></li>
							<li><a href="#" data-filter=".landing" @click="sortItems($event)">Landing Pages</a></li>
							<li><a href="#" data-filter=".corporate" @click="sortItems($event)">Corporate & Business</a></li>
							<li><a href="#" data-filter=".ecommerce" @click="sortItems($event)">E-commerce</a></li>
							<li><a href="#" data-filter=".mobile" @click="sortItems($event)">Mobile Apps</a></li>
							<li><a href="#" data-filter=".web-apps" @click="sortItems($event)">Complex Web Apps</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div> <!-- end of portfolio section -->
		<div class="grid portfolio-items" >
			<div class="grid-sizer"></div>
			
			<div v-for="project in projects" class=" {{project.category}}" :class="{'grid-item--height2': project.isLong , 'grid-item': true, }">
				<a class="portfolio-items effect-apollo" href="/view.php" @click="openProject(project.title, $event)">
					<img :src="'img/portfolio-projects/' + project.image"/>
					<span class="caption">
						<span class="project-title">{{project.title}}</span>
						<span class="project-descr">{{project.description}}</span>
					</span>
				</a>
			</div>
		</div>
	
		<?php include 'patrials/callout.html'; ?>
		<?php include 'patrials/footer.html'; ?>


	<script src="js/jquery.min.js"></script>
	<script src="js/vue.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/isotope.pkgd.min.js"></script>
	<script src="js/portfolio.js"></script>
	</body>
</html>	