<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Astwellsoft</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Teko:400,300,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- vendor css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- custom css -->
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/media.css">
	</head>
	<body>
			<!--[if lt IE 8]>
					<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			<![endif]-->
		<nav class="navbar navbar-default navbar-fixed-top">
		      <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/logo.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="portfolio.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contacts</a></li>

          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>
    <div id="fullPage">
    <section class="hero-unit section">
    	<div class="mask"></div>
    	<div class="container">
    		<div class="row">
		    	<div class="hero-unit-callout">
						<p>ASTWELLSOFT is a software development company that’s easy to work with.</p>    	 
						<p>we’re stable, reliable, and trusted worldwide.</p>
						<div class="hero-unit-actions">
							<a href="/portfolio.php" class="btn btn-primary">Review Portfolio</a><a href="/contact.php" class="btn btn-primary">Go to Contacts</a>
						</div>    	 
					</div>
    			<div class="hero-mouse">
    				<a href="#services"><img src="img/mouse.png"></a>
    				<p>Discover more</p>
    			</div>
    		</div>
    	</div>
    </section>
    <section class="services section" id="services">
    	<div class="left-bg">
    		<img src="img/2-inverted.png">
    	</div>
    	<div class="right-bg">
    		<img src="img/1-inverted.png">
    	</div>
    	<div class="container">
					<h1>Services</h1>
  				<div class="row">
    			<div class="services-grid clearfix">
    				<div class="col-md-4 col-sm-6">
    					<a href="/services.php" class="services-grid-item clearfix">
    						<div class="item-left"><span class="icon"></span></div>
    						<div class="item-right"><span class="text">Business Systems & tools</span></div>			
    					</a>
    				</div>
    				<div class="col-md-4 col-sm-6">
    					<a href="/services.php" class="services-grid-item">
    						<div class="item-left">
    							<span class="icon"></span>
    						</div>		    				
    						<div class="item-right">
    							<span class="text">E-COMMERCE PLATFORMS</span>
    						</div>    					
    					</a>
  					</div>
  					<div class="col-md-4 col-sm-6">
  						<a href="/services.php" class="services-grid-item">
  							<div class="item-left">
  								<span class="icon"></span>
  							</div>			    			
  							<div class="item-right">
  								<span class="text">SOCIAL <br> PLATFORMS</span>
  							</div>    					
  						</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">mobile <br> applications</span>
								</div>    					
							</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">DIGITAL <br> DESIGN</span>
								</div>    					
							</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">SERVERS <br>& DATA</span>
								</div>    					
							</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">Technical support for  STARTUPS</span>
								</div>    					
							</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">SUPPORT OF EXISTING PROJECTS</span>
								</div>    					
							</a>
						</div>
						<div class="col-md-4 col-sm-6">
							<a href="/services.php" class="services-grid-item">
								<div class="item-left">
									<span class="icon"></span>
								</div>			    			
								<div class="item-right">
									<span class="text">Identity & <br> Branding</span>
								</div>    					
							</a>
						</div>						
					</div>
					</div>  			
			</div> <!-- container -->
			<section class="explore">
				<div class="text-center">
					<h3>PLEASE GO TO PORTFOLIO TO REVIEW SOME OF OUR LATEST WORKS</h3>
					<a href="/portfolio.html" class="btn btn-primary"> Review Portfolio</a>
				</div>
			</section>			
		</section>
		<section class="testimonials section"> 
			<div class="container">
				<div class="row">
					<h1>Testimonials</h1>
					<div class="map-container text-center">
							<div class="tooltip slide0">
								<span class="tooltip-item"></span>
							</div>
							<div class="tooltip slide1">
								<span class="tooltip-item"></span>
							</div>
							<div class="tooltip slide2">
								<span class="tooltip-item"></span>
							</div>
							<div class="tooltip slide3">
								<span class="tooltip-item"></span>
							</div>
							<div class="tooltip slide4">
								<span class="tooltip-item"></span>
							</div>
							<div class="tooltip slide5">
								<span class="tooltip-item"></span>
							</div>																		
							<img src="img/map.png">
					</div>
<!-- 					<div class="quote-slider" id="owl">
						<div class="quote-slider-content">
							<p class="main-text"> We would recommend Astwellsoft to anyone who wants to hire a team of developers.</p>
							<p class="sub-text">Alexei Kounine, CEO, TasteHit</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text"> Fantastic experience. Work was done very quick, and the result exceeded my expectations.</p>
							<p class="sub-text">Phil Bucy, Business owner, TreasuresOfTroy</p>
						</div>	
						<div class="quote-slider-content">
							<p class="main-text">We were happy with the result. We would work with them again.</p>
							<p class="sub-text">Arthur Spruiker, Admin, Sourcelegal</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Very professional programmers.</p>
							<p class="sub-text">Natalia Fernandez Garzon, E-commerce & Marketing Manager, Barcelonaled</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Great design and front end code as always. Thanks</p>
							<p class="sub-text">Gladys Kenfack, Product owner, Awoola</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Great on all accounts. Will use again - communication for an off-shore team was waaay better than average.</p>
							<p class="sub-text">Alex Zeig, CEO, Bluecheck</p>
						</div>															
					</div> -->
					<div class="quote-slider" id="owl">
						<div class="quote-slider-content">
							<p class="main-text"> We would recommend Astwellsoft to anyone who wants to hire a team of developers.</p>
							<p class="sub-text">Alexei Kounine, CEO, TasteHit</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text"> Fantastic experience. Work was done very quick, and the result exceeded my expectations.</p>
							<p class="sub-text">Phil Bucy, Business owner, TreasuresOfTroy</p>
						</div>	
						<div class="quote-slider-content">
							<p class="main-text">We were happy with the result. We would work with them again.</p>
							<p class="sub-text">Arthur Spruiker, Admin, Sourcelegal</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Very professional programmers.</p>
							<p class="sub-text">Natalia Fernandez Garzon, E-commerce & Marketing Manager, Barcelonaled</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Great design and front end code as always. Thanks</p>
							<p class="sub-text">Gladys Kenfack, Product owner, Awoola</p>
						</div>
						<div class="quote-slider-content">
							<p class="main-text">Great on all accounts. Will use again - communication for an off-shore team was waaay better than average.</p>
							<p class="sub-text">Alex Zeig, CEO, Bluecheck</p>
						</div>															
					</div>					
				</div>
			</div>
		</section>
		<section class="contacts section">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>Contact us</h1>
						<form id="contact-message">
							<div class="form-group">
								<input type="text" name="name" placeholder="Your Name">
							</div>
							<div class="form-group">
								<input type="text" name="email" placeholder="Your Email">
							</div>
							<div class="form-group">
								<textarea name="message" placeholder="Please Enter Your Message"></textarea>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-default">Send Message</button>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<h1>Visit us</h1>
						<div class="tab-container">

						  <!-- Tab panes -->
						  <div class="tab-content">
						  	<div class="tab-mask"></div>
						    <div role="tabpanel" class="tab-pane  active" id="office-1">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Mississauga, ON, L4W 5B2, Canada</p>
						    	<p>2600 Skymark Ave</p>
						    	<p>+1 416 262 0774</p>						    	
						    </div>
						    <div role="tabpanel" class="tab-pane" id="office-2">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Lviv, Ukraine</p>
						    	<p>73 Heroyiv Upa St., Office 302, 79000</p>
						    	<p>+38 098 343 9808</p>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="office-3">
						    	<p class="text-center"><img src="img/mark.png"></p>
						    	<p>Chernivtsi, Ukraine</p>
						    	<p>2 Koziubynskoho St., 58000</p>
						    	<p>+38 096 444 45 43</p>
						    </div>
						  </div>	

						  <!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active"><a href="#office-1" aria-controls="office-1" role="tab" data-toggle="tab">Toronto, Canada</a></li>
						    <li role="presentation" ><a href="#office-2" aria-controls="office-2" role="tab" data-toggle="tab">Lviv, Ukraine</a></li>
						    <li role="presentation"><a href="#office-3" aria-controls="office-3" role="tab" data-toggle="tab">Chernivtsi, Ukraine</a></li>
						  </ul>
						
						</div>
					</div>
				</div> <!--row -->
				
				<?php include 'patrials/social-block.html'; ?>

				<div class="footer main-page-footer">
					<div class="footer-small">
						<div class="panel panel-default">
							<div class="text-right">
								<p>&copy; 2015 Astwellsoft</p>
							</div>
						</div>				
					</div> <!-- footer-small -->
				</div>
			</div>
		</section>

  </div>

	<script src="js/jquery.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-scrollto.js"></script>
	<script src="js/jquery.fullPage.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/app.js"></script>
	<script src="js/send-form.js"></script>
	</body>
</html>	